import React, { Component } from 'react';
import Quotes from "./containers/Quotes/Quotes";
import AddQuote from "./containers/AddQuote/AddQuote";
import NavBar from "./components/NavBar/NavBar";
import EditQuote from "./containers/EditQuote/EditQuote";
import NavCategories from "./components/NavCategories/NavCategories";
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import './App.css';


import { library } from '@fortawesome/fontawesome-svg-core'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faTimes } from '@fortawesome/free-solid-svg-icons'

library.add(faEdit,faTimes);



class App extends Component {
  render() {
    return (
        <BrowserRouter>
            <div className="App">
              <NavBar/>
              <NavCategories/>
              <div className="container">
                  <Switch>
                      <Route path="/" exact component={Quotes}/>
                      <Route path="/addquote" exact component={AddQuote}/>
                      <Route path="/quotes/:id/edit"  component={EditQuote}/>
                      <Route path="/quotes/:categoryId" component={Quotes}/>
                  </Switch>
              </div>
            </div>
        </BrowserRouter>
    );
  }
}

export default App;
