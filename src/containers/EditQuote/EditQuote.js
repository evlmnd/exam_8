import React, {Component} from 'react';
import QuoteForm from "../../components/QuoteForm/QuoteForm";
import axios from "../../axios-quotes";

import './EditQuote.css';

class EditQuote extends Component {

    editQuote = quote => {
        const id = this.props.match.params.id;
        axios.put('quotes/' + id + '.json', quote).then(() => {
            this.props.history.replace('/');
        })
    };

    render() {
        return (
            <div>
                <QuoteForm
                    quoteId={this.props.match.params.id}
                    submit={this.editQuote}
                    heading="Edit Quote"
                    button="Edit"
                />
            </div>
        );
    }
}

export default EditQuote;