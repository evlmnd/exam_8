import React, {Component} from 'react';
import Quote from "../../components/Quote/Quote";
import axios from '../../axios-quotes';
import {CATEGORIES} from "../../constants";

import './Quotes.css';

class Quotes extends Component {

    state = {
        quotes: {}
    };

    editQuote = event => {
        event.preventDefault();
        this.props.history.push({
            pathname: 'quotes/' + event.currentTarget.id
        })
    };

    removeQuote = event => {
        event.preventDefault();

        axios.delete('quotes/' + event.currentTarget.id + '.json').catch(error => {
            console.log(error);
        }).finally(() => {
            this.props.history.go('/');
        });
    };

    loadData() {
        let url = 'quotes.json';
        const categoryId = this.props.match.params.categoryId;
        if (categoryId) {
            url += `?orderBy="category"&equalTo="${categoryId}"`
        }

        axios.get(url).then(response => {
            this.setState({quotes: response.data});
        }).catch(error => {
            console.log(error);
        });
    }

    componentDidMount() {
        this.loadData()
    }

    componentDidUpdate(prevprops) {
        if (this.props.match.params.categoryId !== prevprops.match.params.categoryId) {
            this.loadData()
        }


    }

    render() {

        let quotes = [];

        if (Object.keys(this.state.quotes).length > 0) {

            for (const key in this.state.quotes) {
                const quote = this.state.quotes[key];

                quotes.push(
                    <Quote
                        text={quote.text}
                        author={quote.author}
                        editClick={this.editQuote}
                        removeClick={this.removeQuote}
                        key={key}
                        id={key}
                    />
                )
            }
        } else {
            quotes = "Nothing here yet :(";
        }

        let heading = 'All';

        if (this.props.match.params.categoryId) {
            for (let i = 0; i < CATEGORIES.length; i++) {
                if (CATEGORIES[i].id === this.props.match.params.categoryId) {
                    heading = CATEGORIES[i].title;
                }
            }
        }


        return (
            <div className="Quotes">
                <h3>{heading}</h3>
                {quotes}
            </div>
        );
    }
}

export default Quotes;