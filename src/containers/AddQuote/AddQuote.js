import React, {Component} from 'react';
import QuoteForm from "../../components/QuoteForm/QuoteForm";
import axios from '../../axios-quotes';

import './AddQuote.css';

class AddQuote extends Component {

    addQuote = quote => {

        if (quote.author === '') {
            quote.author = 'Unknown Author';
        }

        if (quote.text === '') {
            alert('Please complete the text field');
        } else {
            axios.post('quotes.json', quote).finally(() => {
                this.props.history.push('/');
            })
        }
    };

    render() {
        return (
            <div>
                <QuoteForm
                    submit={this.addQuote}
                    heading="Add Quote"
                    button="Add"
                />
            </div>
        );
    }
}

export default AddQuote;