import React, {Component} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import './Quote.css';

class Quote extends Component {
    render() {
        return (
            <div className="Quote">
                <p className="Text">"{this.props.text}"</p>
                <p className="Author">{this.props.author}</p>
                <div className="Buttons-Box">
                    <FontAwesomeIcon icon="edit" onClick={this.props.editClick} id={this.props.id + '/edit'}
                                     className="Quote-Icon"/>
                    <FontAwesomeIcon icon="times" onClick={this.props.removeClick} id={this.props.id}
                                     className="Quote-Icon"/>
                </div>
            </div>
        );
    }
}

export default Quote;