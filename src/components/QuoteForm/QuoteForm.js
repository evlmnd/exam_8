import React, {Component} from 'react';
import axios from '../../axios-quotes';
import {CATEGORIES} from '../../constants';
import './QuoteForm.css';


class QuoteForm extends Component {

    state = {
        category: CATEGORIES[0].id,
        author: '',
        text: ''
    };

    changeCategory = event => {
        let category = null;
        for (let i = 0; i < CATEGORIES.length; i++) {
            if (CATEGORIES[i].title === event.target.value) {
                category = CATEGORIES[i].id;
            }
        }
        console.log(category);
        this.setState({category});
    };

    changeAuthor = event => {
        const author = event.target.value;
        this.setState({author});
    };

    changeText = event => {
        const text = event.target.value;
        this.setState({text});
    };

    submitHandler = event => {
        event.preventDefault();
        const quote = {...this.state};
        this.props.submit(quote)
    };

    componentDidMount() {
        if (this.props.quoteId) {
            axios.get('quotes/' + this.props.quoteId + '.json').then(response => {
                this.setState(response.data);
            }).catch(error => {
                console.log(error);
            });
        }
    }

    render() {

        const options = CATEGORIES.map(category => {
            return <option key={category.id} id={category.id}>{category.title}</option>
        });


        return (
            <div className="Quote-Form">
                <h4>{this.props.heading}</h4>
                <form onSubmit={this.submitHandler}>
                    <label>Category</label>
                    <select className="Input" onChange={this.changeCategory}>
                        {options}
                    </select>

                    <label>Author</label>
                    <input className="Input" type="text"
                           onChange={this.changeAuthor}
                           value={this.state.author}
                    />

                    <label>Quote text</label>
                    <textarea className="Input" name="quote-text" cols="30" rows="10"
                              onChange={this.changeText}
                              value={this.state.text}
                    />
                    <button>{this.props.button}</button>
                </form>
            </div>
        );
    }
}

export default QuoteForm;