import React, {Component} from 'react';
import NavLink from "react-router-dom/es/NavLink";

import './NavBar.css';

class NavBar extends Component {
    render() {
        return (
            <div className="Nav-Bar">
                <div className="Nav-Box">
                    <h2 className="Logo">Quotes.Central</h2>
                    <ul>
                        <li><NavLink to="/">Quotes</NavLink></li>
                        <li><NavLink to="/addquote">Add Quote</NavLink></li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default NavBar;