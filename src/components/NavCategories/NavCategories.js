import React, {Component} from 'react';
import NavLink from "react-router-dom/es/NavLink";
import {CATEGORIES} from '../../constants';

import './NavCategories.css';

class NavCategories extends Component {
    render() {
        return (
            <div className="Nav-Categories">
                <ul>
                    <li><NavLink to="/">All</NavLink></li>
                    {CATEGORIES.map(category => {
                        return <li key={category.id}><NavLink exact to={"/quotes/" + category.id}>{category.title}</NavLink></li>
                    })}
                </ul>
            </div>
        );
    }
}

export default NavCategories;