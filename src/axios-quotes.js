import axios from 'axios';


const instance = axios.create({
    baseURL: 'https://exam-8-somik.firebaseio.com/'
});

export default instance;